﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuadEquationWinForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            double a, b, c, descr, x1 = 0, x2 = 0;
            a = double.Parse(textBox_a.Text);
            b = double.Parse(textBox_b.Text);
            c = double.Parse(textBox_c.Text);
            descr = Math.Pow(b, 2) - (4 * a * c);
            if (descr > 0)
            {
                x1 = ((-b + Math.Sqrt(descr)) / (2 * a));
                x2 = ((-b - Math.Sqrt(descr)) / (2 * a));
            }
            else 
            if (descr == 0)
            {
                x1 = (-b / (2 * a));
            }



            if (descr > 0)
            {
                textBox_x1.Visible = true;
                textBox_x2.Visible = true;
                lable_x1.Visible = true;
                lable_x2.Visible = true;
                textBox_desc.Text = descr.ToString();
                textBox_x1.Text = x1.ToString();
                textBox_x2.Text = x2.ToString(); 
            }
            else if (descr == 0)
            {
                textBox_x1.Visible = true;
                lable_x1.Visible = true;
                textBox_desc.Text = descr.ToString();
                textBox_x1.Text = x1.ToString();
                textBox_x2.Visible = false;
                lable_x2.Visible = false;
            }
            else
            {
                textBox_desc.Text = descr.ToString();
                textBox_x1.Visible = false;
                textBox_x2.Visible = false;
                lable_x1.Visible = false;
                lable_x2.Visible = false;
                MessageBox.Show("Рівняння коренів немає");
            }
        }
    }
}
