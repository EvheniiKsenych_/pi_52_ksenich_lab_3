﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuadEquationWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            double a, b, c, descr, x1 = 0, x2 = 0;
            a = double.Parse(textBox_a.Text);
            b = double.Parse(textBox_b.Text);
            c = double.Parse(textBox_c.Text);
            descr = Math.Pow(b, 2) - (4 * a * c);

            if (descr > 0)
            {
                x1 = ((-b + Math.Sqrt(descr)) / (2 * a));
                x2 = ((-b - Math.Sqrt(descr)) / (2 * a));
            }
            else if (descr == 0)
            {
                x1 = (-b / (2 * a));
            }



            if (descr > 0)
            {
                textBox_x1.Visibility = Visibility.Visible;
                textBox_x1.Visibility = Visibility.Visible;
                label_x2.Visibility = Visibility.Visible;
                label_x2.Visibility = Visibility.Visible;
                textBox_desc.Text = descr.ToString();
                textBox_x1.Text = x1.ToString();
                textBox_x2.Text = x2.ToString();
            }
            else if (descr == 0)
            {
                textBox_x1.Visibility = Visibility.Visible;
                label_x1.Visibility = Visibility.Visible;
                textBox_desc.Text = descr.ToString();
                textBox_x1.Text = x1.ToString();
                textBox_x2.Visibility = Visibility.Hidden;
                label_x2.Visibility = Visibility.Hidden;
            }
            else
            {
                textBox_desc.Text = descr.ToString();
                textBox_x1.Visibility = Visibility.Hidden;
                textBox_x2.Visibility = Visibility.Hidden;
                label_x1.Visibility = Visibility.Hidden;
                label_x2.Visibility = Visibility.Hidden;
                MessageBox.Show("Рівняння коренів немає");
            }
        }
    }
}
